;;;; $Id: rfc2822.asd,v 1.7 2003/10/15 15:58:08 eenge Exp $
;;;; $Source: /project/rfc2822/cvsroot/rfc2822/rfc2822.asd,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the LICENSE file for licensing information.

(in-package #:cl-user)

(defpackage #:rfc2822-system
    (:use #:cl #:asdf)
  (:documentation "Package to create the ASDF system for rfc2822
package"))

(defpackage #:rfc2822-test-system
    (:use #:cl #:asdf)
  (:documentation "Package to create the ASDF system for the
rfc2822-test package"))

(in-package #:rfc2822-system)

(defsystem rfc2822
    :name "rfc2822"
    :author "Erik Enge"
    :version "0.1.0"
    :licence "MIT"
    :description "Implementation of RFC2822"
    :long-description "RFC 2822 specifies a syntax for text messages
that are sent between computer users, within the framework of
\"electronic mail\" messages.  This is an implementation of that RFC."
    :depends-on (:cl-ppcre)
    :properties ((#:author-email . "erik@nittin.net")
                 (#:date . "$Date: 2003/10/15 15:58:08 $")
                 ((#:albert #:output-dir) . "doc/api-doc/")
                 ((#:albert #:formats) . ("docbook"))
                 ((#:albert #:docbook #:template) . "book")
                 ((#:albert #:docbook #:bgcolor) . "white")
                 ((#:albert #:docbook #:textcolor) . "black"))
    :components ((:file "package")
                 (:file "common"
                        :depends-on ("package"))
                 (:file "regex"
                        :depends-on ("package"))
                 (:file "field"
                        :depends-on ("package" "regex" "common"))
                 (:file "date"
                        :depends-on ("package" "regex"))
                 (:file "address"
                        :depends-on ("package" "regex" "common"))
                 (:file "message"
                        :depends-on ("package" "field" "date" "regex" "common"))))
                                               
(in-package #:rfc2822-test-system)

(require :rt)

(defsystem rfc2822-test
    :name "rfc2822-test"
    :author "Erik Enge"
    :version "0.1.0"
    :licence "MIT"
    :description "Tests for the rfc2822 package"
    :depends-on (:rfc2822 :rt)
    :components ((:file "package")
                 (:file "test-common" :pathname "test/test-common"
                        :depends-on ("package"))
                 (:file "test-date" :pathname "test/test-date"
                        :depends-on ("package"))
                 (:file "test-address" :pathname "test/test-address"
                        :depends-on ("package"))))
