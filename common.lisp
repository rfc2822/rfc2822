;;;; $Id: common.lisp,v 1.1.1.1 2003/08/13 14:03:52 eenge Exp $
;;;; $Source: /project/rfc2822/cvsroot/rfc2822/common.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

;;;
;;; Code common to several files
;;;

(in-package :rfc2822)

(define-condition rfc2822-error (error)
  ()
  (:documentation "Base condition for all RFC2822 conditions"))

(defun wash (string)
  "Remove characters not supposed to be in string, according to
RFC2822.  These are currently ASCII codes 13 (carriage-return) and 10
\(linefeed) if they are not together and followed by at least one 32
\(space).  In addition, remove all carriage-return+linefeeds."
  (cl-ppcre:regex-replace-all "\\r\\n?|\\n" string ""))

;; needs to handle the case where length > 78 and 998
(defun fold-unstructured (string &key (new-string "") (start-column 0) (fold-at 78))
  "Fold string"
  (if (equal string "")
      new-string
      (let* ((nearest-fws (find-fws string (- fold-at start-column)))
             (str-len (length string))
             (fws-or-end (min (or nearest-fws 0) str-len)))
        (if (< str-len fold-at)
            (concatenate 'string new-string string)
            (fold-unstructured (subseq string fws-or-end)
             :new-string (concatenate 'string new-string (subseq string 0 fws-or-end)))))))

(defun find-fws (string pos)
  "Return position of the nearest folding-white-space character.  If
no fws characters are found it will return nil."
  (let* ((pos (min (length string) pos))
         (fws-pos (max (or (position #\Tab string :end pos :from-end t) 0)
                       (or (position #\Space string :end pos :from-end t) 0))))
    (when (and (> pos 0)
               (> fws-pos 0))
      fws-pos)))

(defun list-to-string (list)
  (with-output-to-string (out)
    (dolist (string list)
      (write-string string out))))

(defun file-as-string (filename)
  (with-output-to-string (out)
    (let ((buf (make-array 4096 :element-type 'character)))
      (with-open-file (in filename :direction :input :element-type 'character)
        (loop for pos = (read-sequence buf in)
              while (> pos 0)
              do (write-sequence buf out :end pos))))))