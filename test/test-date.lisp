;;;; $Id: test-date.lisp,v 1.7 2003/07/18 02:10:38 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/test/test-date.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822-test)

(deftest parse-day-of-week.1 (rfc2822:parse-day-of-week "Mon, 20 Jul 2003 10:45:21 +0006") "Mon")
(deftest parse-day-of-week.2 (rfc2822:parse-day-of-week "20 Jul 2003 10:45:21 +0006") nil)

(deftest parse-day.1 (rfc2822:parse-day "Mon, 20 Jul 2003 10:45:21 +0006") 20 2)
(deftest parse-day.2 (rfc2822:parse-day "20 Jul 2003 10:45:21 +0006") 20 2)

(deftest parse-month.1 (rfc2822:parse-month "Mon, 20 Jul 2003 10:45:21 +0006") "Jul")
(deftest parse-month.2 (rfc2822:parse-month "20 Jul 2003 10:45:21 +0006") "Jul")

(deftest parse-year.1 (rfc2822:parse-year "Mon, 20 Jul 2003 10:45:21 +0006") 2003 4)
(deftest parse-year.2 (rfc2822:parse-year "20 Jul 2003 10:45:21 +0006") 2003 4)

(deftest parse-second.1 (rfc2822:parse-second "Mon, 20 Jul 2003 10:45:21 +0006") 21 2)
(deftest parse-second.2 (rfc2822:parse-second "20 Jul 2003 10:45:21 +0006") 21 2)

(deftest parse-minute.1 (rfc2822:parse-minute "Mon, 20 Jul 2003 10:45:21 +0006") 45 2)
(deftest parse-minute.2 (rfc2822:parse-minute "20 Jul 2003 10:45:21 +0006") 45 2)

(deftest parse-hour.1 (rfc2822:parse-hour "Mon, 20 Jul 2003 10:45:21 +0006") 10 2)
(deftest parse-hour.2 (rfc2822:parse-hour "20 Jul 2003 10:45:21 +0006") 10 2)

(deftest parse-zone.1 (rfc2822:parse-zone "Mon, 20 Jul 2003 10:45:21 +0006") 6 5)
(deftest parse-zone.2 (rfc2822:parse-zone "Mon, 20 Jul 2003 10:45:21 -0006") -6 5)

(deftest parse-time.1 (rfc2822:parse-time "Mon, 20 Jul 2003 10:45:21 +0006") "10:45:21 +0006")

(deftest write-object-dt.1 (rfc2822:write-object (rfc2822:parse-date "Mon, 20 Jul 2003 10:45:21 +0006"))
  "Mon, 20 Jul 2003 10:45:21 +0006
")
(deftest write-object-dt.2 (rfc2822:write-object (rfc2822:parse-date "20 Jul 2003 10:45:21 +0006"))
  "20 Jul 2003 10:45:21 +0006
")
(deftest write-object-dt.3 (rfc2822:write-object (rfc2822:parse-date "Mon, 20 Jul 2003 10:45:21 -0006"))
  "Mon, 20 Jul 2003 10:45:21 -0006
")

