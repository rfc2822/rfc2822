;;;; $Id: test-address.lisp,v 1.8 2003/07/18 02:10:38 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/test/test-address.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822-test)

(deftest parse-addr-spec.1 (rfc2822:parse-addr-spec "erik@nittin.net") "erik@nittin.net")
(deftest parse-addr-spec.2 (rfc2822:parse-addr-spec "<erik@nittin.net>") "erik@nittin.net")
(deftest parse-addr-spec.3 (rfc2822:parse-addr-spec "Erik Enge erik@nittin.net") "erik@nittin.net")
(deftest parse-addr-spec.4 (rfc2822:parse-addr-spec "Erik Enge <erik@nittin.net>") "erik@nittin.net")
(deftest parse-addr-spec.5 (rfc2822:parse-addr-spec "\"Erik Enge\" <erik@nittin.net>") "erik@nittin.net")

(deftest parse-domain.1 (rfc2822:parse-domain "erik@nittin.net") "nittin.net")
(deftest parse-domain.2 (rfc2822:parse-domain "<erik@nittin.net>") "nittin.net")
(deftest parse-domain.3 (rfc2822:parse-domain "Erik Enge erik@nittin.net") "nittin.net")
(deftest parse-domain.4 (rfc2822:parse-domain "Erik Enge <erik@nittin.net>") "nittin.net")
(deftest parse-domain.5 (rfc2822:parse-domain "\"Erik Enge\" <erik@nittin.net>") "nittin.net")

(deftest parse-local-part.1 (rfc2822:parse-local-part "erik@nittin.net") "erik")
(deftest parse-local-part.2 (rfc2822:parse-local-part "<erik@nittin.net>") "erik")
(deftest parse-local-part.3 (rfc2822:parse-local-part "Erik Enge erik@nittin.net") "erik")
(deftest parse-local-part.4 (rfc2822:parse-local-part "Erik Enge <erik@nittin.net>") "erik")
(deftest parse-local-part.5 (rfc2822:parse-local-part "\"Erik Enge\" <erik@nittin.net>") "erik")
(deftest parse-local-part.6 (rfc2822:parse-local-part "\"Erik Enge\" <erik.enge@nittin.net>") "erik.enge")

(deftest parse-display-name.1 (rfc2822:parse-display-name "erik@nittin.net") nil)
(deftest parse-display-name.2 (rfc2822:parse-display-name "<erik@nittin.net>") nil)
(deftest parse-display-name.3 (rfc2822:parse-display-name "Erik Enge erik@nittin.net") "Erik Enge")
(deftest parse-display-name.4 (rfc2822:parse-display-name "Erik Enge <erik@nittin.net>") "Erik Enge")
(deftest parse-display-name.5 (rfc2822:parse-display-name "\"Erik Enge\" <erik@nittin.net>") "\"Erik Enge\"")

(deftest domain-literal-p.1 (rfc2822:domain-literal-p (rfc2822:parse-address "erik@nittin.net")) nil)
(deftest domain-literal-p.2 (rfc2822:domain-literal-p (rfc2822:parse-address "\"Erik Enge\" <erik@[192.168.1.1]>")) t)

(deftest parse-group-display-name.1 (rfc2822:parse-group-display-name "A group: Erik Enge <erik@nittin.net>;") "A group")
(deftest parse-group-display-name.2 (rfc2822:parse-group-display-name "A group: Erik Enge <erik@nittin.net>, Thomas Stenhaug <thomas@src.no>;") "A group")
(deftest parse-group-display-name.3 (rfc2822:parse-group-display-name "onewordgroup: Erik Enge <erik@nittin.net>;") "onewordgroup")

(deftest parse-addresses.1 (rfc2822:parse-addresses "A group: Erik Enge <erik@nittin.net>;")
  ("Erik Enge <erik@nittin.net>"))
(deftest parse-addresses.2 (rfc2822:parse-addresses "A group: Erik Enge <erik@nittin.net>, Thomas Stenhaug <thomas@src.no>;")
  ("Erik Enge <erik@nittin.net>" "Thomas Stenhaug <thomas@src.no>"))
