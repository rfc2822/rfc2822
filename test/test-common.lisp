;;;; $Id: test-common.lisp,v 1.8 2003/07/15 00:09:38 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/test/test-common.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822-test)

(deftest wash.1 (rfc2822:wash "") "")
(deftest wash.2 (rfc2822:wash "short string") "short string")
(deftest wash.3 (rfc2822:wash "a very long string that should not change at all from being exposed to a little bit of washing...")
  "a very long string that should not change at all from being exposed to a little bit of washing...")
(deftest wash.4 (rfc2822:wash (format nil "a string with something ~Ain i~At that should be washed" #\Return #\Linefeed))
  "a string with something in it that should be washed")
(deftest wash.5 (rfc2822:wash (format nil "a string that ~A~A should be changed after washing" #\Return #\Linefeed))
  "a string that  should be changed after washing")
(deftest wash.6 (rfc2822:wash (format nil "a string that aoeu~A~Aaoeu be changed after washing" #\Return #\Linefeed))
  "a string that aoeuaoeu be changed after washing")
