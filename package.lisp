;;;; $Id: package.lisp,v 1.22 2003/08/04 21:32:22 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/package.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the LICENSE file for licensing information.

(in-package :cl-user)

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defpackage :rfc2822
      (:use :cl)
    (:export #:make-field-name
             #:contents
             #:valid-p
             #:write-object
             #:field-name
             #:invalid-field-name
             #:invalid-field-body
             #:make-field
             #:subject
             #:comments
             #:fold
             #:unfold
             #:wash
             #:parse-msg-ids
             ; date specific functions
             #:parse-date
             #:parse-day-of-week
             #:parse-day
             #:parse-month
             #:parse-year
             #:parse-time
             #:parse-second
             #:parse-minute
             #:parse-hour
             #:parse-zone
             ; address specific functions
             #:parse-addr-spec
             #:parse-domain
             #:parse-local-part
             #:parse-display-name
             #:parse-address
             #:parse-potential-address
             #:domain-literal-p
             #:parse-group-display-name
             #:parse-group
             #:parse-addresses
             ; message specific functions
             #:address-count
             #:remove-address
             #:add-address
             #:parse-file
             #:parse-directory
             #:file-as-string)
    (:documentation "An implementation of RFC2822"))
  (defpackage :rfc2822-test
      (:use :cl :rt)
    (:documentation "Tests to validate the RFC2822 implementation")))


