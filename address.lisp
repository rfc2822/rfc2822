;;;; $Id: address.lisp,v 1.29 2003/08/04 21:32:22 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/address.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the LICENSE file for licensing information.

(in-package :rfc2822)

;;;
;;; Condition
;;;

(define-condition not-an-address (rfc2822-error)
  ((address
    :reader address
    :initarg :address))
  (:report (lambda (condition stream)
             (format stream "String is not an address: ~A." (address condition))))
  (:documentation "Represents the exceptional situation that occurs
when the string supplied is not an address and thus cannot be parsed
accordingly."))

(define-condition not-a-group (rfc2822-error)
  ((group
    :reader group
    :initarg :group))
  (:report (lambda (condition stream)
             (format stream "String is not a group: ~A." (group condition))))
  (:documentation "Represents the exceptional situation that occurs
when the string supplied is not a group and thus cannot be parsed
accordingly."))

;;
;; Generic function
;;

(defgeneric domain-literal-p (object)
  (:documentation "Does the object contain a domain-literal?"))

(defgeneric add-address (group address)
  (:documentation "Add address to group"))

(defgeneric remove-address (group address)
  (:documentation "Remove address from group"))

(defgeneric address-count (group)
  (:documentation "Return the number of addresses this group holds"))

;;
;; Address
;;

(defclass address ()
  ((local-part
    :accessor local-part
    :initarg :local-part
    :documentation "The local-part portion is a domain dependent
string.  In addresses, it is simply interpreted on the particular host
as a name of a particular mailbox.")
   (domain
    :accessor domain
    :initarg :domain
    :documentation"The domain portion identifies the point to which the
mail is delivered.")
   (display-name
    :accessor display-name
    :initarg :display-name
    :initform nil
    :documentation "The name of the addressee."))
  (:documentation "A mailbox receives mail.  It is a conceptual entity
which does not necessarily pertain to file storage.  For example, some
sites may choose to print mail on a printer and deliver the output to
the addressee's desk.  Normally, a mailbox is comprised of two parts:
\(1) an optional display name that indicates the name of the recipient
\(which could be a person or a system) that could be displayed to the
user of a mail application, and (2) an addr-spec address enclosed in
angle brackets (\"<\" and \">\")."))

(defmethod domain-literal-p ((address address))
  "Does the address contain a domain-literal?"
  (if (cl-ppcre:scan (get-scanner :domain-literal) (domain address))
      t
      nil))

(defun parse-addr-spec (string)
  "Parse addr-spec from string"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :addr-spec) string)
    (if (and start end)
        (string-trim " " (subseq string start end))
        nil)))

(defun parse-domain (string)
  "Parse domain from string"
  (let ((addr-spec (parse-addr-spec string)))
    (if addr-spec
        (string-trim " " (second (cl-ppcre:split "@" addr-spec)))
        nil)))

(defun parse-local-part (string)
  "Parse local-part from string"
  (let ((addr-spec (parse-addr-spec string)))
    (if addr-spec
        (string-trim " " (first (cl-ppcre:split "@" addr-spec)))
        nil)))

(defun parse-display-name (string)
  "Parse display-name from string"
  (let ((addr-start (cl-ppcre:scan (get-scanner :addr-spec) string)))
    (if addr-start
        (progn
          (when (cl-ppcre:scan "<.+>" string)
            (setq addr-start (1- addr-start)))
          (let ((string (string-trim " " (subseq string 0 addr-start))))
            (if (equal string "")
                nil
                string)))
        nil)))

(defun parse-address (string)
  "Return an address object"
  (if (cl-ppcre:scan (get-scanner :address) string)
      (make-instance 'address
                     :local-part (parse-local-part string)
                     :domain (parse-domain string)
                     :display-name (parse-display-name string))
      (error (make-condition 'not-an-address :address string))))

(defmethod write-object ((address address) &key (stream nil))
  "Pretty-print address"
  (if (display-name address)
      (format stream "~A <~A@~A>"
              (display-name address)
              (local-part address)
              (domain address))
      (format stream "<~A@~A>"
              (local-part address)
              (domain address))))

;;
;; Group
;;

(defclass group ()
  ((display-name
    :accessor display-name
    :initarg :display-name
    :documentation "The display-name of the group")
   (addresses
    :accessor addresses
    :initarg :addresses
    :initform '()
    :documentation "A list of address objects representing the
addresses contained in this group."))
  (:documentation "When it is desirable to treat several mailboxes as
a single unit (i.e., in a distribution list), the group construct can
be used.  The group construct allows the sender to indicate a named
group of recipients. This is done by giving a display name for the
group, followed by a colon, followed by a comma separated list of any
number of mailboxes (including zero and one), and ending with a
semicolon."))

(defmethod write-object ((group group) &key (stream nil))
  "Pretty-print group"
  (if (display-name group)
      (format stream "~A:~{ ~A~^,~};"
              (display-name group)
              (mapcar #'write-object (addresses group)))
      (format stream "Nogroupname:~{ ~A~};"
              (mapcar #'write-object (addresses group)))))

(defmethod add-address ((group group) (address address))
  "Add address to group"
  (push address (addresses group)))

(defmethod remove-address ((group group) (address address))
  "Remove address to group"
  (setf (addresses group) (remove address (addresses group))))

(defmethod address-count ((group group))
  "Return the number of address this group holds"
  (length (addresses group)))

(defun parse-group-display-name (string)
  "Parse group display-name from string"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :display-name) string)
    (if (and start end)
        (string-trim " " (subseq string start end))
        nil)))

(defun parse-addresses (string)
  "Return a list of address strings"
  (mapcar #'(lambda (string)
              (string-trim " " string))
          (cl-ppcre:all-matches-as-strings (get-scanner :mailbox) string)))

(defun parse-group (string)
  "Return an group object"
  (let ((group (make-instance
                'group
                :display-name (parse-group-display-name string))))
    (dolist (address (reverse (mapcar #'parse-address (parse-addresses (text-in-field string)))))
      (add-address group address))
    group))

;;
;; Utility function
;;

(defun parse-potential-address (string)
  "Will check string to see if it contains group, etc. and then call
the appropriate parsing function."
  (if (cl-ppcre:scan (get-scanner :is-group) string)
      (parse-group string)
      (parse-address string)))
