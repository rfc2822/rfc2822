;;;; $Id: regex.lisp,v 1.44 2003/08/04 21:32:22 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/regex.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822)

(let* (; ASCII 13 followed by ASCII 10
       (crlf "\\r\\n")
       ; ASCII 32 (space) or ASCII 9 (horizontal tab)
       (wsp "\\s")
       ; ([*WSP CRLF] 1*WSP) / obs-FWS
       (fws (format nil "(?:~A*~A)?~A+" wsp crlf wsp))
       ; %d1-8 / %d11 / %d12 / %d14-31 / %d127
       (no-ws-ctl "\\x01-\\x08|\\x0B|\\x0C|\\x0E-\\x1F|\\x7F")
       ; NO-WS-CTL / %d33-39 / %d42-91 / %d93-126
       (ctext (format nil "(?:~A)|[\\x21-\\x27\\x2A-\\x5B\\x5D-\\x7E]" no-ws-ctl))
       ; %d1-9 / %d11 / %d12 / %d14-127 / obs-text
       (text "\\x01-\\x09|\\x0B|\\x0C|\\x0E-\\x7F")
       ; ("\" text) / obs-qp
       (quoted-pair (format nil "\\\\(?:~A)+" text))
       ; "(" *([FWS] ccontent) [FWS] ")"
       (comment (format nil "\\(((?:~A)?((?:~A)|(?:~A)))(?:~A)?\\)"
                        fws ctext quoted-pair fws)) ; this does not deal with nested comments yet
       ; ctext / quoted-pair / comment
       (ccontent (format nil "(?:~A)|(?:~A)|(?:~A)" ctext quoted-pair comment)) ; shouldn't this be used in comment?
       ;  *([FWS] comment) (([FWS] comment) / FWS)
       (cfws (format nil "((?:~A)?(?:~A))*(((?:~A)?(?:~A))|(?:~A))" fws comment fws comment fws))
       ; NO-WS-CTL / %d33-126 / obs-utext
       (utext (format nil "(?:~A)|[\\x21-\\x7E]" no-ws-ctl))
       ; *([FWS] utext) [FWS]
       (unstructured (format nil "((?:~A)?(?:~A))*(?:~A)?" fws utext fws))
       ; (( "+" / "-" ) 4DIGIT) / obs-zone
       (zone "(\\+|-)\\d{4}")
       ; 2DIGIT / obs-second
       (second "\\d{2}")
       ; 2DIGIT / obs-minute
       (minute "\\d{2}")
       ; 2DIGIT / obs-hour
       (hour "\\d{2}")
       ; hour ":" minute [ ":" second ]
       (time-of-day (format nil "~A:~A(?::~A)?" hour minute second))
       ; time-of-day FWS zone
       (time (format nil "(?:~A)(?:~A)(?:~A)" time-of-day fws zone))
       ; ([FWS] 1*2DIGIT) / obs-day
       (day "\\d{1,2}")
       ; "Jan" / "Feb" / "Mar" / "Apr" / "May" / "Jun" / "Jul" / "Aug" /
       ; "Sep" / "Oct" / "Nov" / "Dec"
       (month-name "Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec")
       ; (FWS month-name FWS) / obs-month
       (month month-name)
       ;(month (format nil "(?:~A)(~A)(?:~A)" fws month-name fws))
       ; 4*DIGIT / obs-year
       (year "\\d{4}")
       ; day month year
       (date (format nil "(?:~A)(?:~A)(?:~A)(?:~A)~A" day fws month fws year))
       ; "Mon" / "Tue" / "Wed" / "Thu" / "Fri" / "Sat" / "Sun"
       (day-name "Mon|Tue|Wed|Thu|Fri|Sat|Sun")
       ; ([FWS] day-name) / obs-day-of-week
       (day-of-week (format nil "(?:~A)?(?:~A)" fws day-name))
       ; [ day-of-week "," ] date FWS time [CFWS]
       (date-time (format nil "(~A,(?:~A)?)?(?:~A)(?:~A)(?:~A)(?:~A)?" day-of-week fws date fws time cfws))
       ; NO-WS-CTL / %d33-90 / %d94-126        
       (dtext (format nil "(?:~A)|[\\x21-\\x5B]|[\\x5E-x7E]" no-ws-ctl))
       ; dtext / quoted-pair
       (dcontent (format nil "(?:~A)|(?:~A)" dtext quoted-pair))
       ; [CFWS] "[" *([FWS] dcontent) [FWS] "]" [CFWS]
       (domain-literal (format nil "(?:~A)?\\[((?:~A)?(?:~A))*(?:~A)?\\](?:~A)?" cfws fws dcontent fws cfws))
       ; ALPHA
       (alpha (format nil "[a-z]|[A-Z]"))
       ; DIGIT
       (digit (format nil "[0-9]"))
       ; ALPHA / DIGIT / "!" / "#" / "$" / "%" / "&" / "'" / "*" / "+" /
       ; "-" / "/" / "=" / "?" / "^" / "_" / "`" / "{" / "|" / "}" / "~"
       (atext (format nil "((?:~A)|~A|[-!#$%&'*+/=?^_`{|}~~])" alpha digit))
       ; [CFWS] 1*atext [CFWS]
       (atom (format nil "((?:~A)?~A+(?:~A)?)" cfws atext cfws))
       ; 1*atext *("." 1*atext)
       (dot-atom-text (format nil "~A+(\\.~A+)*" atext atext))
       ; [CFWS] dot-atom-text [CFWS]
       (dot-atom (format nil "(?:~A)?(?:~A)(?:~A)?" cfws dot-atom-text cfws))
       ; dot-atom / domain-literal / obs-domain
       (domain (format nil "(?:~A)|(?:~A)" dot-atom domain-literal))
       ; NO-WS-CTL / %d33 / %d35-91 / %d93-126 
       (qtext (format nil "(?:~A)|[\\x21\\x23-\\x5B\\x5D-\\x7E]" no-ws-ctl))
       ; qtext / quoted-pair
       (qcontent (format nil "((?:~A)|(?:~A))" qtext quoted-pair))
       ; DQUOTE (unsure about this one)
       (dquote (format nil "\"")) ; I'm not sure if this is correct
       ; [CFWS] DQUOTE *([FWS] qcontent) [FWS] DQUOTE [CFWS]
       (quoted-string (format nil "((?:~A)?~A((?:~A)?~A)*(?:~A)?~A(?:~A)?)" cfws dquote fws qcontent fws dquote cfws))
       ; dot-atom / quoted-string / obs-local-part 
       (local-part (format nil "((?:~A)|~A)" dot-atom quoted-string))
       ; local-part "@" domain
       (addr-spec (format nil "~A@(?:~A)" local-part domain))
       ; atom / quoted-string
       (word (format nil "~A|~A" atom quoted-string))
       ; 1*word / obs-phrase
       (phrase (format nil "(?:~A)+" word))
       ; phrase
       (display-name phrase)
       ; [CFWS] "<" addr-spec ">" [CFWS] / obs-angle-addr
       (angle-addr (format nil "((?:~A)?<(~A)>(?:~A)?)" cfws addr-spec cfws))
       ; [display-name] angle-addr
       (name-addr (format nil "(?:~A?(?:~A)~A)" display-name fws angle-addr))
       ; name-addr / addr-spec
       (mailbox (format nil "~A|(?:~A)" name-addr addr-spec))
       ; (mailbox *("," mailbox)) / obs-mbox-list
       (mailbox-list (format nil "(?:~A)(?:,~A)*" mailbox mailbox))
       ; display-name ":" [mailbox-list / CFWS] ";" [CFWS]
       (group (format nil "~A:(?:(?:~A)|(?:~A))?;(?:~A)?" display-name mailbox-list cfws cfws))
       (is-group (format nil "~A:.*;" display-name))
       ; mailbox / group
       (address (format nil "(~A)|~A" mailbox group))
       ; (address *("," address)) / obs-addr-list
       (address-list (format nil "(~A)(,(~A))*" address address))
       ; DQUOTE *(qtext / quoted-pair) DQUOTE
       (no-fold-quote (format nil "(~A((?:~A)|(?:~A))*~A)" dquote qtext quoted-pair dquote))
       ; "[" *(dtext / quoted-pair) "]"
       (no-fold-literal (format nil "(\\[((?:~A)|(?:~A))*\\])" dtext quoted-pair))
       ; dot-atom-text / no-fold-literal / obs-id-right
       (id-right (format nil "((~A)|~A)" dot-atom-text no-fold-literal))
       ; dot-atom-text / no-fold-quote / obs-id-left
       (id-left (format nil "((~A)|~A)" dot-atom-text no-fold-quote))
       ; [CFWS] "<" id-left "@" id-right ">" [CFWS]
       (msg-id (format nil "(?:~A)?<~A@~A>(?:~A)?" cfws id-left id-right cfws))
       ; 1*msg-id
       (in-reply-to (format nil "(?:~A)+" msg-id))
       ; 1*msg-id
       (references (format nil "(?:~A)+" msg-id))
       ; 1*angle-addr / addr-spec / atom / domain / msg-id
       (item-value (format nil "(~A+|(~A)|~A|(~A)|(~A))" angle-addr addr-spec atom domain msg-id))
       ; ALPHA *(["-"] (ALPHA / DIGIT))
       (item-name (format nil "((?:~A)(-?((?:~A)|~A))*)" alpha alpha digit))
       ; item-name CFWS item-value
       (name-val-pair (format nil "(~A(?:~A)~A)" item-name cfws item-value))
       ; [CFWS] [name-val-pair *(CFWS name-val-pair)]
       (name-val-list (format nil "((?:~A)?(~A((?:~A)~A)*)?)" cfws name-val-pair cfws name-val-pair))
       ; ([CFWS] "<" ([CFWS] / addr-spec) ">" [CFWS]) / obs-path
       (path (format nil "((?:~A)?<((?:~A)?|(~A))>(?:~A)?)" cfws cfws addr-spec cfws))
       (semi-empty-line "(?<=\\n)\\n")
       (field-name-syntax "^[\\x21-\\x39\\x3B-\\x7E]+$")
       (field-name "^[\\x21-\\x39\\x3B-\\x7E]+:")
       (field-name-not-start "[\\x21-\\x39\\x3B-\\x7E]+:")
       (regexes
        (list (list :field-syntax (format nil "~A.*\\n( .*\\n)*" field-name-not-start))
              (list :crlf crlf)
              (list :wsp wsp)
              (list :fws fws)
              (list :cfws cfws)
              (list :no-ws-ctl no-ws-ctl)
              (list :utext utext)
              (list :unstructured unstructured)
              (list :zone zone)
              (list :second second)
              (list :minute minute)
              (list :hour hour)
              (list :time-of-day time-of-day)
              (list :time time)
              (list :day day)
              (list :month-name month-name)
              (list :month month)
              (list :year year)
              (list :date date)
              (list :day-name day-name)
              (list :day-of-week day-of-week)
              (list :date-time date-time)
              (list :mailbox-list mailbox-list)
              (list :mailbox mailbox)
              (list :name-addr name-addr)
              (list :group group)
              (list :is-group is-group)
              (list :address address)
              (list :address-list address-list)
              (list :angle-addr angle-addr)
              (list :display-name display-name)
              (list :addr-spec addr-spec)
              (list :local-part local-part)
              (list :domain domain)
              (list :domain-literal domain-literal)
              (list :atom atom)
              (list :dot-atom dot-atom)
              (list :dot-atom-text dot-atom-text)
              (list :msg-id msg-id)
              (list :references references)
              (list :in-reply-to in-reply-to)
              (list :field-name-syntax field-name-syntax)
              (list :field-name field-name)
              (list :field-name-not-start field-name-not-start)
              (list :path path)
              (list :name-val-list name-val-list)
              (list :semi-empty-line semi-empty-line))))
  (declare (ignore ccontent))
  (defun get-regex (regex-designator)
    "Returns a pcre-style regex"
    (cadr (assoc regex-designator regexes)))
  (defun get-regexes ()
    "Return all pcre-style regexes"
    regexes))

(let ((scanners '()))
  (dolist (regex-alist (get-regexes))
    (push (list (car regex-alist)
                (cl-ppcre:create-scanner
                 (cadr regex-alist)
                 :case-insensitive-mode t))
          scanners))
  (defun get-scanner (regex-designator)
    "Return a scanner built with the regex designated by
regex-designator."
    (cadr (assoc regex-designator scanners)))
  (defun get-scanners ()
    "Return all scanners"
    scanners))

