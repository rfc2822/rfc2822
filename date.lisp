;;;; $Id: date.lisp,v 1.22 2003/08/04 21:32:22 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/date.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

;;;; As you will see, if have chosen "sec" and "time-" instead of
;;;; "second" and "time"; the reason is that CL-USER already defines
;;;; these and that CMUCL complains about it.  I think the real fix is
;;;; to not :use :cl.  I'll do that once I'm done with the code,
;;;; because I don't really like having to do cl-user:.

(in-package :rfc2822)

;;
;; Date
;;

(let* ((month-symbol '((:january "Jan")
                       (:february "Feb")
                       (:march "Mar")
                       (:april "Apr")
                       (:may "May")
                       (:june "Jun")
                       (:july "Jul")
                       (:august "Aug")
                       (:september "Sep")
                       (:october "Oct")
                       (:november "Nov")
                       (:december "Dec")
                       (nil nil)))
       (day-symbol '((:monday "Mon")
                     (:tuesday "Tue")
                     (:wednesday "Wed")
                     (:thursday "Thu")
                     (:friday "Fri")
                     (nil nil)))
       (month-string (mapcar #'reverse month-symbol))
       (day-string (mapcar #'reverse day-symbol)))
  (defun get-month-string (symbol)
    (second (assoc symbol month-symbol)))
  (defun get-month-symbol (string)
    (second (assoc string month-string :test #'equal)))
  (defun get-day-string (symbol)
    (second (assoc symbol day-symbol)))
  (defun get-day-symbol (string)
    (second (assoc string day-string :test #'equal))))

(defclass date ()
  ((zone
    :accessor zone
    :initarg :zone
    :documentation "The zone specifies the offset from Coordinated
Universal Time (UTC, formerly referred to as \"Greenwich Mean Time\")
that the date and time-of-day represent.  The \"+\" or \"-\" indicates
whether the time-of-day is ahead of (i.e., east of) or behind (i.e.,
west of) Universal Time.  The first two digits indicate the number of
hours difference from Universal Time, and the last two digits indicate
the number of minutes difference from Universal Time.  (Hence, +hhmm
means +(hh * 60 + mm) minutes, and -hhmm means -(hh * 60 + mm)
minutes).  The form \"+0000\" SHOULD be used to indicate a time zone
at Universal Time.  Though \"-0000\" also indicates Universal Time, it
is used to indicate that the time was generated on a system that may
be in a local time zone other than Universal Time and therefore
indicates that the date-time contains no information about the local
time zone.")
   (seconds
    :accessor seconds
    :initarg :seconds
    :initform nil)
   (minutes
    :accessor minutes
    :initarg :minutes
    :initform nil)
   (hours
    :accessor hours
    :initarg :hours
    :initform nil)
   (day
    :accessor day
    :initarg :day
    :documentation "The day is the numeric day of the month.")
   (month-name
    :accessor month-name
    :initarg :month-name)
   (year
    :accessor year
    :initarg :year
    :documentation "The year is any numeric year 1900 or later.")
   (day-of-week
    :accessor day-of-week
    :initarg :day-of-week
    :initform nil))
  (:documentation "Represents an RFC2822 date"))

(defun parse-day-of-week (string)
  "Get day-of-week from string or nil"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :day-of-week) (string-trim " " string))
    (if (and start end)
        (subseq string start end)
        nil)))

(defun parse-day (string)
  "Get day from string or nil"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :day) string)
    (if (and start end)
        (parse-integer (subseq string start end))
        nil)))

(defun parse-month (string)
  "Get month from string or nil"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :month) string)
    (if (and start end)
        (subseq string start end)
        nil)))

(defun parse-year (string)
  "Get year from string or nil"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :year) string)
    (if (and start end)
        (parse-integer (subseq string start end))
        nil)))

(defun parse-second (time-string)
  "Get second from string (parsed by parse-time)"
  (let ((list (cl-ppcre:split ":" time-string)))
    (when (third list)
      (parse-integer (first (cl-ppcre:split " " (third list)))))))

(defun parse-minute (time-string)
  "Get minute from string (parsed by parse-time)"
  (let ((list (cl-ppcre:split ":" time-string)))
    (parse-integer (second list))))

(defun parse-hour (time-string)
  "Get hour from string (parsed by parse-time)"
  (let ((list (cl-ppcre:split ":" time-string)))
    (parse-integer (car (last (cl-ppcre:split " " (first list)))))))

(defun parse-zone (string)
  "Get zone from string or nil"
  (let ((time (parse-time string)))
    (when time
      (multiple-value-bind (start end)
          (cl-ppcre:scan (get-scanner :zone) time)
        (if (and start end)
            (parse-integer (subseq time start end))
            nil)))))
              
(defun parse-time (string)
  "Get time from string or nil"
  (multiple-value-bind (start end)
      (cl-ppcre:scan (get-scanner :time) string)
    (if (and start end)
        (subseq string start end)
        nil)))

(defun parse-date (string)
  "Parse string and make a date from it."
  (let ((time (parse-time string)))
    (if time
        (make-instance 'date
                       :zone (parse-zone string)
                       :seconds (parse-second time)
                       :minutes (parse-minute time)
                       :hours (parse-hour time)
                       :day (parse-day string)
                       :month-name (get-month-symbol (parse-month string))
                       :year (parse-year string)
                       :day-of-week (get-day-symbol (parse-day-of-week string)))
        (make-instance 'date
                       :zone (parse-zone string)
                       :day (parse-day string)
                       :month-name (get-month-symbol (parse-month string))
                       :year (parse-year string)
                       :day-of-week (get-day-symbol (parse-day-of-week string))))))

(defmethod write-object ((date date) &key (stream nil))
  "Write representation of object to stream or return string"
  (let ((pp-zone (format nil "~A~4,'0d" (if (minusp (zone date))
                                            "-"
                                            "+")
                         (if (minusp (zone date))
                             (* (zone date) -1)
                             (zone date))))
        (pp-hours (format nil "~2,'0d" (hours date)))
        (pp-minutes (format nil "~2,'0d" (minutes date)))
        (pp-seconds (format nil "~2,'0d" (seconds date))))
    (cond
      ((and (seconds date) (day-of-week date))
       (format stream "~A, ~A ~A ~A ~A:~A:~A ~A~%"
               (get-day-string (day-of-week date))
               (day date)
               (get-month-string (month-name date))
               (year date)
               pp-hours
               pp-minutes
               pp-seconds
               pp-zone))
      ((seconds date)
       (format stream "~A ~A ~A ~A:~A:~A ~A~%"
               (day date)
               (get-month-string (month-name date))
               (year date)
               pp-hours
               pp-minutes
               pp-seconds
               pp-zone))
      ((day-of-week date)
       (format stream "~A, ~A ~A ~A ~A:~A ~A~%"
               (get-day-string (day-of-week date))
               (day date)
               (get-month-string (month-name date))
               (year date)
               pp-hours
               pp-minutes
               pp-zone)))))
