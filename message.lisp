;;;; $Id: message.lisp,v 1.1.1.1 2003/08/13 14:03:52 eenge Exp $
;;;; $Source: /project/rfc2822/cvsroot/rfc2822/message.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822)

;;
;; Message
;;

(defclass message ()
  ((fields
    :accessor fields
    :initform '()
    :initarg :fields
    :type list)
   (body
    :accessor body
    :initarg :body
    :type string))
  (:documentation "A message consists of header fields, optionally
followed by a message body.  Lines in a message MUST be a maximum of
998 characters excluding the CRLF, but it is RECOMMENDED that lines be
limited to 78 characters excluding the CRLF.  (See section 2.1.1 for
explanation.)  In a message body, though all of the characters listed
in the text rule MAY be used, the use of US-ASCII control characters
\(values 1 through 8, 11, 12, and 14 through 31) is discouraged since
their interpretation by receivers for display is not guaranteed."))

(defun make-message (&optional (fields nil) (body ""))
  "Return a message instance."
  (make-instance 'message :fields fields :body body))

(defun parse-message (string)
  "Parse string and make a message from it."
  (let ((two-parts (split-message string))
        (message (make-message)))
    (dolist (field (reverse (split-fields (car two-parts))))
      (let ((type (type-of-field field))
            (text (text-in-field field)))
        (if (parse-function-p type)
            (add-field message (make-field :type type
                                           :body (funcall (parse-function type) text)))
            (add-field message (make-field :type type :body text)))))
    (setf (body message) (cadr two-parts))
    message))

(defun parse-file (filename)
  "Open and read file and return a message object."
  (parse-message (file-as-string filename)))

(defun parse-directory (directory)
  "Parse all files in given directory and return a list of message
objects."
  (mapcar #'parse-file (directory directory)))

(defun split-message (string)
  "Split message into field and body part."
  (cl-ppcre:split (get-scanner :semi-empty-line) string))

(defun split-fields (string)
  "Split string of fields into a list of fields."
  (cl-ppcre:all-matches-as-strings (get-scanner :field-syntax) string))

(defmethod write-object ((message message) &key (stream nil))
  "Pretty print object to stream or return a string."
  (format stream "~{~A~}~%~A"
          (mapcar #'write-object (fields message))
          (or (body message) "")))

(defmethod add-field ((message message) (field field))
  "Add field to message."
  (push field (fields message)))

(defmethod add-fields ((message message) fields)
  "Add field to message."
  (mapc #'(lambda (field)
            (add-field message field)) fields))

(defmethod get-field ((message message) field-type)
  "Return field from message."
  (dolist (field (fields message))
    (when (eq (type field) field-type)
      (return-from get-field field))))

(defmethod remove-field ((message message) field-type)
  "Remove field of type 'field-type' from message.  If there are
several fields of the same type, remove all of them."
  (setf (fields message)
        (remove-if #'(lambda (field)
                       (eq (type field) field-type)) (fields message))))

(defmethod print-object ((object message) stream)
  "Print the object for the Lisp reader."
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "with ~D field~:P" (length (fields object)))))

;;
;; Utility
;;

(defun create-message-id (&key (local-part nil) (domain nil))
  "Create a message-id that is relatively unique.  I have semi-adopted
\(I am not sure how well my randomness is\) the method described by
Jamie Zawinski and Matt Curtin in their expired Internet Draft
available at <http://www.jwz.org/doc/mid.html>."
  (let* ((*print-base* 36)
         (wall-clock (format nil "~A" (get-universal-time)))
         (randomness (format nil "~A" (random 99999999)))
         (*print-base* 10))
    (format nil "<~A@~A>"
            (or local-part (format nil "~A.~A" wall-clock randomness))
            (or domain (machine-instance)))))
            
