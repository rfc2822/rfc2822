;;;; $Id: field.lisp,v 1.67 2003/08/04 21:32:22 erik Exp $
;;;; $Source: /home/cvsd/repo/rfc2822/field.lisp,v $

;;;; Authored by Erik Enge, <erik@nittin.net>.
;;;; 
;;;; See the COPYING file for licensing information.

(in-package :rfc2822)

;;;
;;; Generic function
;;;

(defgeneric write-object (object &key stream)
  (:documentation "Print object to stream or return string"))

;;;
;;; Field
;;;

(defclass field ()
  ((type
    :accessor type
    :initarg :type
    :type symbol)
   (body
    :accessor body
    :initarg :body
    :type string))
  (:documentation "Header fields are lines composed of a field name,
followed by a colon (\":\"), followed by a field body, and terminated
by CRLF.  A field name MUST be composed of printable US-ASCII
characters (i.e., characters that have values between 33 and 126,
inclusive), except colon.  A field body may be composed of any
US-ASCII characters, except for CR and LF.  However, a field body may
contain CRLF when used in header \"folding\" and \"unfolding\" as
described in section 2.2.3.  All field bodies MUST conform to the
syntax described in sections 3 and 4 of this standard."))

(defun make-field (&key (type nil) (body nil))
  "Return a field instance."
  (make-instance 'field :type type :body body))

(defun type-of-field (string)
  "Return a symbol that designates what type of field the string is."
  (intern (string-upcase
           (string-trim " " (car (cl-ppcre:split ":" string)))) :keyword))

;; a parse-function is a function to call for fields that might
;; require extra parsing.  for instance, the To field might be a group
;; instead of a plain address and we need to be able to determine
;; this. EE20020722
(let ((types '((:to parse-potential-address)
               (:cc parse-potential-address)
               (:bcc parse-potential-address)
               (:resent-from parse-potential-address)
               (:resent-to parse-potential-address)
               (:resent-cc parse-potential-address)
               (:resent-bcc parse-potential-address)
               (:resent-sender parse-potential-address)
               (:sender parse-potential-address)
               (:from parse-potential-address))))
  (defun parse-function (type)
    "Return function to parse fields of type with."
    (cadr (assoc type types))))

(defun parse-function-p (type)
  "Does type have an associated parse-function?"
  (if (parse-function type)
      t
      nil))

(defun text-in-field (string)
  "Return the text part of the field (remove field-name)."
  (wash (string-trim
         " "
         (list-to-string
          (cdr (cl-ppcre:split (get-scanner :field-name) string))))))

(defmethod write-object ((object field) &key (stream nil))
  "Pretty print object to stream or return a string."
  (let* ((field-name (string-capitalize (symbol-name (type object))))
         (field-body (if (stringp (body object))
                         (fold-unstructured
                          (wash (body object))
                          :start-column (+ (length field-name) 2))
                         (write-object (body object)))))
    (format stream "~A: ~A~%" field-name field-body  )))

(defmethod print-object ((object field) stream)
  "Print the object for the Lisp reader."
  (print-unreadable-object (object stream :type t :identity t)
    (princ (type object) stream)))
