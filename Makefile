# $Id: Makefile,v 1.2 2003/07/14 23:43:58 erik Exp $
# $Source: /home/cvsd/repo/rfc2822/Makefile,v $

clean:
	find -name "*.fasl" -o -name "*.faslmt" -o -name "*~" -o -name "*.err" -o -name "*.x86f" | xargs rm 

commit:
	make clean; cvs up; cvs ci

